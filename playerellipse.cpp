#include "playerellipse.h"

PlayerEllipse::PlayerEllipse(int x, int y, int w, int h) :
	QGraphicsEllipseItem(x,y,w,h)
{
}
void PlayerEllipse::update()
{
	QRectF thisRect = this->rect();
	this->setRect(QRectF(thisRect.x() + PLAYERS_SPEED * playerNextMovement[0], thisRect.y() + PLAYERS_SPEED * playerNextMovement[1], PLAYERS_SIZE, PLAYERS_SIZE));

	thisRect = this->rect();

	qreal plx = thisRect.x();
	qreal ply = thisRect.y();

	if (plx < 0)
	{
		thisRect.setX(0);	
	}
	if (plx + this->rect().width() > 1000)
	{
		thisRect.setX(1000 - thisRect.width());
	}
	if (ply < 0)
	{
		thisRect.setY(0);
	}
	if (ply + this->rect().height() > 500)
	{
		thisRect.setY(500 - thisRect.height());
	}
	this->setRect(thisRect);
}

void PlayerEllipse::setXMovement(int x)
{
	playerNextMovement[0] = x;
}

void PlayerEllipse::setYMovement(int y)
{
	playerNextMovement[1] = y;
}