#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qline.h>
#include <random>
#include <time.h>




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	srand(time(NULL));
    ui->setupUi(this);
	this->setFixedSize(1000, 500);

    timer = new QTimer();
    connect(this->timer,SIGNAL(timeout()),this,SLOT(update()));
    timer->start(10);

	createEllipses();
	createPlayers();

	this->setMouseTracking(true);

	mediaPlayer.setMedia(QUrl::fromLocalFile("D:/Documents/Projects/QT_Projects/Bilute/Media/Swalla.wav"));
	mediaPlayer.setVolume(50);
	mediaPlayer.play();
}

void MainWindow::moveEvent(QMoveEvent *e) //move window to move ellipse
{
	//QMainWindow::moveEvent(e);
	//QRect r = geometry();
	//playerEllipse->setX(r.x());
	//playerEllipse->setY(r.y());
}

void MainWindow::keyPressEvent(QKeyEvent* e)
{
	switch (e->key()) {
		case Qt::Key::Key_W:
			playerEllipses[0]->setYMovement(-1);
			break;
		case Qt::Key::Key_A:
			playerEllipses[0]->setXMovement(-1);
			break;
		case Qt::Key::Key_S:
			playerEllipses[0]->setYMovement(1);
			break;
		case Qt::Key::Key_D:
			playerEllipses[0]->setXMovement(1);
			break;
		case Qt::Key::Key_Up:
			playerEllipses[1]->setYMovement(-1);
			break;
		case Qt::Key::Key_Left:
			playerEllipses[1]->setXMovement(-1);
			break;
		case Qt::Key::Key_Down:
			playerEllipses[1]->setYMovement(1);
			break;
		case Qt::Key::Key_Right:
			playerEllipses[1]->setXMovement(1);
			break;
		default:
			break;
		}
}

void MainWindow::keyReleaseEvent(QKeyEvent* e)
{
	switch (e->key()) {
	case Qt::Key::Key_W:
		playerEllipses[0]->setYMovement(0);
		break;
	case Qt::Key::Key_A:
		playerEllipses[0]->setXMovement(0);
		break;
	case Qt::Key::Key_S:
		playerEllipses[0]->setYMovement(0);
		break;
	case Qt::Key::Key_D:
		playerEllipses[0]->setXMovement(0);
		break;
	case Qt::Key::Key_Up:
		playerEllipses[1]->setYMovement(0);
		break;							
	case Qt::Key::Key_Left:				
		playerEllipses[1]->setXMovement(0);
		break;
	case Qt::Key::Key_Down:
		playerEllipses[1]->setYMovement(0);
		break;
	case Qt::Key::Key_Right:
		playerEllipses[1]->setXMovement(0);
		break;
	default:
		break;
	}
}



void MainWindow::paintEvent(QPaintEvent *)
{
	paintEllipses();
    paintPlayers();
	paintText();
}

MainWindow::~MainWindow()
{
	for (int i = 0; i < ellipses.size(); i++)
	{
		delete ellipses[i];
	}

    delete timer;
	delete ui;
    for(int i=0;i<playerEllipses.size();i++)
    {
       delete playerEllipses[i];
    }
    playerEllipses.clear();
}

qreal MainWindow::getDistance(QGraphicsEllipseItem& P1, QGraphicsEllipseItem& P2)
{
	QPointF P1Coords, P2Coords;
	QLineF distance;

	P1Coords.setX(P1.rect().x() + P1.rect().width()/2);
	P1Coords.setY(P1.rect().y() + P1.rect().height() / 2);
	P2Coords.setX(P2.rect().x() + P2.rect().width() / 2);
	P2Coords.setY(P2.rect().y() + P2.rect().height() / 2);
	distance.setP1(P1Coords);
	distance.setP2(P2Coords);

	return distance.length();
}

void MainWindow::updateEllipses()
{
    collisions[0] = false;
    collisions[1] = false;

	for (int i = 0; i < ellipses.size(); i++)
	{
		qreal elx = ellipses[i]->rect().x();
		qreal ely = ellipses[i]->rect().y();
     
        qreal distanceToPlayers[10] = {0}; //#define this

        for(int j=0;j<playerEllipses.size();j++)
         {
            distanceToPlayers[j] = getDistance(*ellipses[i], *playerEllipses[j]);
			
			qreal radiusSum = ellipses[i]->rect().width() / 2 + playerEllipses[j]->rect().width() / 2;

            if (distanceToPlayers[j] < radiusSum)
            {
                collisions[j] = true;
                collisionScore[j]++;
                ellipsesVec[i].setX(ellipsesVec[i].x()*(-1));
                ellipsesVec[i].setY(ellipsesVec[i].y()*(-1));
            }
        }

        if (elx + ELLIPSE_SIZE > 1000 || elx<0)
        {
            ellipsesVec[i].setX(ellipsesVec[i].x()*(-1));
        }
        if (ely + ELLIPSE_SIZE > 500 || ely<0)
        {
            ellipsesVec[i].setY(ellipsesVec[i].y()*(-1));
        }

		ellipses[i]->setRect(QRectF(elx + ellipsesVec[i].x(), ely + ellipsesVec[i].y(), ellipses[i]->rect().width(), ellipses[i]->rect().height()));
	}

}

void MainWindow::updatePlayers()
{
	//playerEllipse->setX(mapFromGlobal(QCursor::pos()).x() - 50);
	//playerEllipse->setY(mapFromGlobal(QCursor::pos()).y() - 50);

	for (int i = 0; i < playerEllipses.size(); i++)
	{
		playerEllipses[i]->update();
	}
}

void MainWindow::paintEllipses()
{
	QPainter painter(this);
	QBrush brush = painter.brush();
	brush.setColor(Qt::blue);
	brush.setStyle(Qt::SolidPattern);
	painter.setBrush(brush);

	for (int i = 0; i < ellipses.size(); i++)
	{
		auto test1 = (int)ellipses[i]->rect().x();
		auto test2 = (int)ellipses[i]->rect().y();

		painter.drawEllipse((int)ellipses[i]->rect().x(), (int)ellipses[i]->rect().y(), ELLIPSE_SIZE, ELLIPSE_SIZE);
	}
}

void MainWindow::paintPlayers()
{
	QPainter painter(this);
	QBrush brush = painter.brush();
	brush.setColor(Qt::red);
	brush.setStyle(Qt::SolidPattern);
	painter.setBrush(brush);

    for(int i=0;i<playerEllipses.size();i++)
    {
        if (collisions[i])
        {
            brush.setColor(Qt::magenta);
            painter.setBrush(brush);
        }

        painter.drawEllipse((int)playerEllipses[i]->rect().x(), (int)playerEllipses[i]->rect().y(), PLAYERS_SIZE, PLAYERS_SIZE);

        brush.setColor(Qt::green);
        painter.setBrush(brush);
    }
}

void MainWindow::paintText()
{
	QPainter painter(this);
    QString logText = "P1 Collision Score: " + QString::number(collisionScore[0]);
    logText += "    P2 Collision Score: " + QString::number(collisionScore[1]);
	//logText += "    P1 x,y,w,h: " + QString::number(playerEllipses[0]->rect().x()) + " " + QString::number(playerEllipses[0]->rect().y()) + " " + QString::number(playerEllipses[0]->rect().width()) + " " + QString::number(playerEllipses[0]->rect().height());
	//logText += "    P2 pos: " + QString::number(playerEllipses[1]->rect().x()) + " " + QString::number(playerEllipses[1]->rect().y()) + " " + QString::number(playerEllipses[1]->rect().width()) + " " + QString::number(playerEllipses[1]->rect().height());
	painter.drawText(QPoint(10, 10), logText);
}

void MainWindow::update()
{
    updateEllipses();
    updatePlayers();

	QMainWindow::update();
}

void MainWindow::createPlayers()
{
	for (int i = 0; i<NR_PLAYERS; i++)
	{
		playerEllipses.push_back(new PlayerEllipse(10,330, PLAYERS_SIZE, PLAYERS_SIZE));
	}
}

void MainWindow::createEllipses()
{
	for (int i = 0; i < NR_ELLIPSES; i++)
	{
		ellipsesVec.push_back(QVector2D());
		ellipsesVec[i].setX(rand() % ELLIPSES_MAX_SPEED + 1);
		ellipsesVec[i].setY(rand() % ELLIPSES_MAX_SPEED + 1);

		ellipses.push_back(new QGraphicsEllipseItem(rand() % 900, rand() % 400, ELLIPSE_SIZE, ELLIPSE_SIZE));
	}
}

