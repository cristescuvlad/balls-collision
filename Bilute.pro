#-------------------------------------------------
#
# Project created by QtCreator 2017-05-14T10:26:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = Bilute
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    playerellipse.cpp

HEADERS  += mainwindow.h \
    playerellipse.h \
    constants.h

FORMS    += mainwindow.ui
