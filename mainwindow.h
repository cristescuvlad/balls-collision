#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QGraphicsEllipseItem>
#include <QPainter>
#include <qvector2d.h>
#include <QMoveEvent>
#include <QMediaPlayer>

#include "playerellipse.h"
#include "constants.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private:
	void paintEvent(QPaintEvent *event);
	void moveEvent(QMoveEvent *e);
	void keyPressEvent(QKeyEvent* e);
	void keyReleaseEvent(QKeyEvent* e);
	qreal getDistance(QGraphicsEllipseItem& P1, QGraphicsEllipseItem& P2);
	void updateEllipses();
    void updatePlayers();
	void paintEllipses();
    void paintPlayers();
	void paintText();

private slots:
    void update();

private:
	void createPlayers();
	void createEllipses();

private:
    Ui::MainWindow *ui;
    QTimer* timer;

    QList<QVector2D> ellipsesVec;
	QList <QGraphicsEllipseItem*> ellipses;
    QList <PlayerEllipse*> playerEllipses;
	QMediaPlayer mediaPlayer;


    bool collisions[2] = {0};
    int collisionScore[2] = {0};
};

#endif // MAINWINDOW_H
