#ifndef PLAYERELLIPSE_H
#define PLAYERELLIPSE_H

#include <qgraphicsitem.h>
#include "constants.h"

class PlayerEllipse : public QGraphicsEllipseItem
{
public:
    PlayerEllipse(int x, int y, int w, int h);
	void setXMovement(int x);
	void setYMovement(int y);
	void update();

private:
	int playerNextMovement[2] = { 0 };

};


#endif // PLAYERELLIPSE_H